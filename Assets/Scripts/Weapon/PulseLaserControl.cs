﻿using UnityEngine;
using System.Collections;

public class PulseLaserControl : MonoBehaviour {

    public float speed = 35;
    public float maxShootDistance = 10000;
    public float damage = 1f;
    public float powerConsumption = 1;

    float _currDistance;

	// Use this for initialization
	void Start () 
    {
        
	}
	
	// Update is called once per frame
	void Update () 
    {
        _currDistance += Time.deltaTime * speed;
        if (_currDistance > maxShootDistance)
        {
            Destroy(gameObject);
        }
        transform.Translate(Vector3.forward * Time.deltaTime * speed);
	}

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("Destructible"))
        {
            if (other.GetComponent<HealthControl>().currHealthPoint > 0)
            {
                other.GetComponent<HealthControl>().HealthChanged(-damage);
                Destroy(gameObject);
            }
        }
    }
}
