﻿using UnityEngine;
using System.Collections;

public class RocketMissileControl : MonoBehaviour 
{

    public GameObject currentTarget;
    public float speed = 120f;
    public bool launched = false;
    public ParticleSystem exhaust;
    public float damage = 2;
    public float maxLifeTime = 5;

    float lifeTime = 0;

	// Use this for initialization
	void Start () 
    {
	    
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (launched)
        {
            transform.Translate(Vector3.forward * Time.deltaTime * speed);
            if (currentTarget != null)
            {
                var targetDirect = currentTarget.transform.position - transform.position;
                transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(targetDirect), 2 * Time.deltaTime);
            }
            lifeTime += Time.deltaTime;
            if (lifeTime >= maxLifeTime)
            {
                Destroy(gameObject);
            }
        }
	}

    public void Launch(GameObject target)
    {
        launched = true;
        currentTarget = target;
        gameObject.transform.SetParent(null);
        exhaust.Play();

    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("Destructible"))
        {
            other.GetComponent<HealthControl>().HealthChanged(-damage);
            Destroy(gameObject);
        }
    }
}
