﻿using UnityEngine;
using System.Collections;

public class TurretControl : MonoBehaviour {

    public bool isActive = false;
    public bool isShooting = false;
    public float fireRate = 0.3f;
    public GameObject projectile;
    public TurretType type;
    public GameObject target;
    public EnergyControl energy;

    float _lastShotTime = 0.0f;
    GameObject _shoot;

    public enum TurretType
    {
        beam, pulse, rocket
    }

	// Use this for initialization
	void Start () 
    {
        if (type == TurretType.rocket)
        {
            MissileReload();
        }
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (isActive && isShooting)
        {
            switch (type)
            {
                case TurretType.beam:
                    BeamShooting();
                    break;
                case TurretType.pulse:
                    PulseShooting();
                    break;
                case TurretType.rocket:
                    RocketLaunch();
                    break;
                default:
                    break;
            }
        }

        if (!isShooting && _shoot != null && type == TurretType.beam)
        {
            Destroy(_shoot);
        }
	}

    public void BeamShooting()
    {
        if (_shoot == null)
        {
            _shoot = (GameObject)Instantiate(projectile, transform.position, transform.rotation);
            _shoot.transform.SetParent(gameObject.transform);
        }
    }

    public void PulseShooting()
    {
        if (Time.time > _lastShotTime)
        {
            _lastShotTime = Time.time + fireRate;
            gameObject.GetComponent<AudioSource>().Play();
            if (energy.ConsumingEnergy(projectile.GetComponent<PulseLaserControl>().powerConsumption))
	        {
                Instantiate(projectile, transform.position, transform.rotation);
	        }
            else
            {
                _shoot = (GameObject)Instantiate(projectile, transform.position, transform.rotation);
                _shoot.GetComponent<MeshRenderer>().materials[0].SetFloat("_LineWidth", 0.5f);
                _shoot.GetComponent<PulseLaserControl>().damage /= 2;
            }
        }
    }

    public void RocketLaunch()
    {
        if (Time.time > _lastShotTime)
        {
            _lastShotTime = Time.time + fireRate;
            _shoot.GetComponent<RocketMissileControl>().Launch(target);
            MissileReload();
        }
    }

    void MissileReload()
    {
        _shoot = (GameObject)Instantiate(projectile, Vector3.zero, new Quaternion());
        _shoot.transform.SetParent(gameObject.transform, false);
    }
}
