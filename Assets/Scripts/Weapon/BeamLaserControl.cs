﻿using UnityEngine;
using System.Collections;

public class BeamLaserControl : MonoBehaviour {

    public GameObject turret;
    public float damage;
    public float powerConsumption = 1f;

    Transform _lastTarget;
    float _currDmg;

    VolumetricLines.VolumetricLineBehavior component;

	// Use this for initialization
	void Start () 
    {
        component = gameObject.GetComponent<VolumetricLines.VolumetricLineBehavior>();
        _currDmg = damage;
	}
	
	// Update is called once per frame
	void Update () 
    {
        RaycastHit hit;

        if (gameObject.GetComponentInParent<TurretControl>().energy.ConsumingEnergy(powerConsumption * Time.deltaTime))
        {
            _currDmg = damage;
            gameObject.GetComponent<MeshRenderer>().materials[0].SetFloat("_LineWidth", 0.5f);
        }
        else
        {
            _currDmg = damage / 2;
            gameObject.GetComponent<MeshRenderer>().materials[0].SetFloat("_LineWidth", 0.25f);
        }
        
        if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hit, 200, 1 << LayerMask.NameToLayer("Destructible")))
        {
            component.m_endPos.z = hit.distance;
            if (_lastTarget == null)
            {
                _lastTarget = hit.transform;
            }
            else
            {
                _lastTarget.GetComponent<HealthControl>().HealthChanged(-_currDmg * Time.deltaTime);
                if (_lastTarget != hit.transform)
                {
                    _lastTarget = hit.transform;
                }
            }
        }
        else
        {
            component.m_endPos.z = 200;
            _lastTarget = null;
        }
	}
}
