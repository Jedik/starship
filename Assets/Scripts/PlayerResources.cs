﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerResources : MonoBehaviour 
{

    public float goldDust = 0;
    public Text show;

	// Use this for initialization
	void Start () 
    {
        //show.transform.GetChild(0).GetComponent<Text>().text = goldDust.ToString();
        
	}
	
	// Update is called once per frame
	void Update () 
    {
        //print(show.transform.childCount);
	}

    public void AddGoldDust(float amount)
    {
        goldDust += amount;
        show.GetComponent<Text>().text = Mathf.Round(goldDust).ToString();
    }
}
