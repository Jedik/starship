﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ObjectsRegistrySystem : MonoBehaviour 
{

    Dictionary<ObjectType, List<GameObject>> sceneObjects = new Dictionary<ObjectType, List<GameObject>>();

    public delegate void MethodContainer(GameObject obj, ObjectType type);
    public event MethodContainer onAddition;

    public enum ObjectType
    {
        Asteroid, GoldDust, NanoBot, NanoBlock, Ship
    }

	// Use this for initialization
	void Start () 
    {
        
	}
	
	// Update is called once per frame
	void Update () 
    {
	
	}

    public void AddObject(ObjectType type, GameObject obj)
    {
        if (!sceneObjects.ContainsKey(type))
        {
            sceneObjects.Add(type, new List<GameObject>());
        }
        sceneObjects[type].Add(obj);
        if (onAddition != null)
        {
            onAddition(obj, type);
        }
    }

    public Dictionary<ObjectType, List<GameObject>> GetAllObjects()
    {
        return sceneObjects;
    }

    public List<GameObject> GetObjectsByType(ObjectType type)
    {
        return sceneObjects[type];
    }

    public void DeleteObject(ObjectType type, GameObject obj)
    {
        sceneObjects[type].Remove(obj);
    }
}
