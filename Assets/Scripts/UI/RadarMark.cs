﻿using UnityEngine;
using System.Collections;

public class RadarMark : MonoBehaviour {

    public GameObject center;
    public GameObject target;
    public float ratio;
    public float maxDistance;

    RectTransform _transform;
    RectTransform _mark;

	// Use this for initialization
	void Start () 
    {
        _transform = gameObject.GetComponent<RectTransform>();
        _transform.anchoredPosition3D = Vector3.zero;
        _mark = gameObject.transform.GetChild(0).GetComponent<RectTransform>();
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (target == null)
        {
            Destroy(gameObject);
        }
        else
        {
            var markPos = center.transform.InverseTransformPoint(target.transform.position);
            markPos *= ratio;

            var radarRadius= gameObject.transform.parent.GetComponent<RectTransform>().rect.width / 2;

            if (markPos.sqrMagnitude > radarRadius * radarRadius)
            {
                _transform.anchoredPosition = new Vector2(markPos.normalized.x * radarRadius, markPos.normalized.z * radarRadius);
                _transform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, 0);
                _mark.anchoredPosition = new Vector2(0, 0);
            }
            else
            {
                if (markPos.y < 0)
                {
                    _transform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, -markPos.y);
                    _transform.anchoredPosition = new Vector2(markPos.x, markPos.z + markPos.y / 2);
                    _mark.anchoredPosition = new Vector2(0, markPos.y / 2);
                }
                else
                {
                    _transform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, markPos.y);
                    _transform.anchoredPosition = new Vector2(markPos.x, markPos.z + markPos.y / 2);
                    _mark.anchoredPosition = new Vector2(0, markPos.y / 2);
                }
            }
        }
	}
}
