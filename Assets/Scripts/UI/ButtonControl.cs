﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.Events;

public class ButtonControl : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{

    public UnityEvent OnUp;
    public UnityEvent OnDown;

	// Use this for initialization
	void Start () 
    {
        var size = transform.GetComponent<RectTransform>();
        var scale = Screen.height / 10;
        size.anchoredPosition = new Vector2(size.anchoredPosition.x / 40 * scale, size.anchoredPosition.y / 40 * scale);
        size.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, scale * 2);
        size.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, scale * 2);
	}
	
	// Update is called once per frame
	void Update () 
    {
        
	}

    public void OnPointerUp(PointerEventData data)
    {
        OnUp.Invoke();
    }

    public void OnPointerDown(PointerEventData data) 
    {
        OnDown.Invoke();
    }

}
