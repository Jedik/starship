﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class JoyZoneControl : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IDragHandler
{

    public Vector3 vMousePos;

    public GameObject AddAim;

    public float _vMouseSensivity = 75;

    Vector2 _center;
    Vector2 _deltaPos;
    Vector3 _vMouseStartPos;

    float smooth = 3;
    float _AddAimDragRadius;
    float _ratio;
    

	// Use this for initialization
	void Start () 
    {
        var size = transform.GetComponent<RectTransform>();
        size.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, Screen.height / 2);
        size.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, Screen.height / 2);
        _center = new Vector2(size.rect.width / 2, size.rect.height / 2);
        _vMouseStartPos = new Vector3((float)Screen.width / 2, (float)Screen.height / 2, 0);
        _AddAimDragRadius = Screen.height / 4;
	}
	
	// Update is called once per frame
	void Update () 
    {
        vMousePos = _vMouseStartPos + ((Vector3)_deltaPos * _vMouseSensivity * _ratio);
        AddAim.transform.position = Vector3.Lerp(AddAim.transform.position, _vMouseStartPos + (Vector3)_deltaPos * _AddAimDragRadius * _ratio, Time.deltaTime * smooth * 8);
	}

    public void OnDrag(PointerEventData data)
    {
        _deltaPos = data.position - _center;
        _ratio = _deltaPos.magnitude / (Screen.height / 4);
        _deltaPos.Normalize();
        if (_ratio > 1)
        {
            _ratio = 1;
        }
    }


    public void OnPointerUp(PointerEventData data)
    {
        _deltaPos = Vector2.zero;
    }

    public void OnPointerDown(PointerEventData data) { }

}
