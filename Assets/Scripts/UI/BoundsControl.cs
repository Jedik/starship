﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BoundsControl : MonoBehaviour 
{
    public Slider hpBar;
    public Slider energyBar;
    public Image deflectionMark;
    public GameObject player;

    GameObject _target;
    //GameObject _targetDeflection;
    HealthControl _targetHP;
    EnergyControl _targetEnergy;

	// Use this for initialization
	void Start () 
    {
        
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (_target != null && _target.GetComponent<Renderer>().isVisible)
        {
            gameObject.GetComponent<RectTransform>().anchoredPosition = (Vector2)Camera.main.WorldToScreenPoint(_target.GetComponent<Renderer>().bounds.center);
            hpBar.value = _targetHP.currHealthPoint / _targetHP.maxHealthPoint;
            if (_target.tag == "Asteroids")
            {
                var temp = (Vector2)Camera.main.WorldToScreenPoint(CalculateDeflection());
                deflectionMark.GetComponent<RectTransform>().anchoredPosition = temp;
            }
        }
        else
        {
            gameObject.GetComponent<RectTransform>().anchoredPosition = new Vector2(-2500, -2500);
            deflectionMark.gameObject.SetActive(false);
        }
	}

    public void TrackTarget(GameObject target)
    {
        _target = target;
        if (_target != null)
        {
            //_targetDeflection = new GameObject("Deflection");
            gameObject.SetActive(true);
            _targetHP = _target.GetComponent<HealthControl>();
            hpBar.gameObject.SetActive(true);
            deflectionMark.gameObject.SetActive(true);
        }
        else
        {
            hpBar.gameObject.SetActive(false);
            deflectionMark.gameObject.SetActive(false);
            gameObject.SetActive(false);
        }
    }

    Vector3 CalculateDeflection()
    {
        var rb = _target.GetComponent<Rigidbody>();
        var distance = (_target.transform.position - player.transform.position).magnitude;

        var result = _target.GetComponent<Renderer>().bounds.center + rb.velocity * (distance / 500);
        
        return result;
    }
    
}
