﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class RadarSystem : MonoBehaviour, IPointerClickHandler
{
    public GameObject ship;
    public float maxDistance;
    public GameObject mark;

    public GameObject marks;
    public GameObject icons;

    public GameObject sceneobjects;
    public int currIcon = 0;

    Transform[] orderedIcons;
    
    

	// Use this for initialization
	void Start () 
    {
        sceneobjects.GetComponent<ObjectsRegistrySystem>().onAddition += AddMark;
        var objects = sceneobjects.GetComponent<ObjectsRegistrySystem>().GetAllObjects();
        foreach (var tMarks in objects)
        {
            foreach (var tMark in tMarks.Value)
            {
                AddMark(tMark, tMarks.Key);
            }
        }

        orderedIcons = new Transform[icons.transform.childCount];
        for (int i = 0; i < icons.transform.childCount; i++)
        {
            switch (icons.transform.GetChild(i).name)
            {
                case "All":
                    orderedIcons[0] = icons.transform.GetChild(i);
                    break;
                case "Asteroids":
                    orderedIcons[1] = icons.transform.GetChild(i);
                    break;
                case "GoldDust":
                    orderedIcons[2] = icons.transform.GetChild(i);
                    break;
                case "NanoBlock":
                    orderedIcons[3] = icons.transform.GetChild(i);
                    break;
                default:
                    break;
            }
        }
	}
	
	// Update is called once per frame
	void Update () 
    {
        //_marks.transform.rotation = Quaternion.Inverse(ship.transform.rotation);
	}

    void AddMark(GameObject obj, ObjectsRegistrySystem.ObjectType type)
    {
        switch (type)
        {
            case ObjectsRegistrySystem.ObjectType.Asteroid:
                if (!(currIcon == 0 || currIcon == 1))
                    return;
                break;
            case ObjectsRegistrySystem.ObjectType.GoldDust:
                if (!(currIcon == 0 || currIcon == 2))
                    return;
                break;
            case ObjectsRegistrySystem.ObjectType.NanoBot:
                if (!(currIcon == 0 || currIcon == 10))
                    return;
                break;
            case ObjectsRegistrySystem.ObjectType.NanoBlock:
                if (!(currIcon == 0 || currIcon == 3))
                    return;
                break;
            case ObjectsRegistrySystem.ObjectType.Ship:
                //if (!(currIcon == 9 || currIcon == 1))
                    return;
                //break;
            default:
                return;
                //break;
        }
        GameObject tmp = (GameObject)Instantiate(mark);
        tmp.transform.SetParent(marks.transform);
        tmp.transform.GetComponent<RadarMark>().center = ship;
        tmp.transform.GetComponent<RadarMark>().target = obj;
        tmp.transform.GetComponent<RadarMark>().ratio = (gameObject.GetComponent<RectTransform>().rect.width / 2) / maxDistance * 2;  
    }

    public void OnPointerClick(PointerEventData data)
    {
        icons.transform.GetChild(currIcon).GetComponent<Image>().color = new Color(255, 255, 255);
        if (currIcon + 1 == icons.transform.childCount)
        {
            currIcon = 0;
        }
        else
        {
            currIcon++;
        }
        icons.transform.GetChild(currIcon).GetComponent<Image>().color = new Color(0, 0.51f, 1);

        for (int i = 0; i < marks.transform.childCount; i++)
        {
            Destroy(marks.transform.GetChild(i).gameObject);
        }

        var objects = sceneobjects.GetComponent<ObjectsRegistrySystem>().GetAllObjects();
        foreach (var tMarks in objects)
        {
            foreach (var tMark in tMarks.Value)
            {
                AddMark(tMark, tMarks.Key);
            }
        }
    }
}


