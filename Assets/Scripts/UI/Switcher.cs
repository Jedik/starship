﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Switcher : MonoBehaviour, IPointerClickHandler
{
    public Sprite beam;
    public Sprite pulse;
    public Sprite rocket;

    enum Type
    {
        beam, pulse, rocket
    }

    Type nextType = Type.beam;

	// Use this for initialization
	void Start () 
    {

	}
	
	// Update is called once per frame
	void Update () 
    {
	
	}

    public void OnPointerClick(PointerEventData eventData)
    {
        //print(nextType);
        //nextType++;
        switch (nextType)
        {
            case Type.beam:
                transform.GetComponent<Image>().sprite = beam;
                transform.GetChild(0).GetComponent<Text>().text = "Beam";
                nextType = Type.rocket;
                break;
            case Type.pulse:
                transform.GetComponent<Image>().sprite = pulse;
                transform.GetChild(0).GetComponent<Text>().text = "Pulse";
                nextType = Type.beam;
                break;
            case Type.rocket:
                transform.GetComponent<Image>().sprite = rocket;
                transform.GetChild(0).GetComponent<Text>().text = "Rocket";
                nextType = Type.pulse;
                break;
            default:
                break;
        }
    }

}
