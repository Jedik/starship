﻿using UnityEngine;
using System.Collections;

public class AimControl : MonoBehaviour {

    public GameObject aimpoint;

	// Use this for initialization
	void Start () 
    {
	
	}
	
	// Update is called once per frame
	void Update () 
    {
        transform.position = Camera.main.WorldToScreenPoint(aimpoint.transform.position);
        transform.position = new Vector3(transform.position.x, transform.position.y, 0);
	}
}
