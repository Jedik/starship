﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class JoyControl : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IDragHandler
{
    public float smooth = 3;

    public Vector3 vMousePos;

    public GameObject AddAim;

    Vector2 _startPos;
    Vector2 _deltaPos;
    Vector3 _vMouseStartPos;

    float _dragRadius = 45f;
    float _vMouseSensivity;
    float _AddAimDragRadius;
    float _ratio;

    void Start()
    {

        var size = transform.parent.GetComponent<RectTransform>();
        size.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Bottom, Screen.height / 10, Screen.height / 4);
        size.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, Screen.height / 10, Screen.height / 4);

        _startPos = transform.position;
        vMousePos = _vMouseStartPos = new Vector3((float)Screen.width / 2, (float)Screen.height / 2, 0);
        _vMouseSensivity = Screen.height / 6;
        _AddAimDragRadius = Screen.height / 4;
    }

    void Update()
    {
        vMousePos = _vMouseStartPos + (Vector3)_deltaPos * _vMouseSensivity * _ratio;
        AddAim.transform.position = Vector3.Lerp(AddAim.transform.position, _vMouseStartPos + (Vector3)_deltaPos * _AddAimDragRadius * _ratio, Time.deltaTime * smooth * 8);
    }

    public void OnDrag(PointerEventData data)
    {
        _deltaPos = data.position - _startPos;
        _ratio = _deltaPos.magnitude / _dragRadius;
        _deltaPos.Normalize();
        
        if (_ratio >= 1)
        {        
            transform.position = _startPos + _deltaPos * _dragRadius;
            _ratio = 1;
        }
        else
        {
            transform.position = new Vector3(data.position.x, data.position.y);
        }
    }


    public void OnPointerUp(PointerEventData data)
    {
        transform.position = _startPos;
        _deltaPos = Vector2.zero;
    }

    public void OnPointerDown(PointerEventData data) {}
}
