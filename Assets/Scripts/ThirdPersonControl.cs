﻿using UnityEngine;
using System.Collections;

public class ThirdPersonControl : MonoBehaviour 
{
    public float distance = 110f;
    public float height = 3.0f;
    public float damping = 15.0f;
    public float rotationDamping = 1.0f;
    public GameObject target;

    Vector3 _wantedPosition;

	// Use this for initialization
	void Start () 
    {
	
	}
	
	// Update is called once per frame
	void Update () 
    {
	
	}

    void LateUpdate()
    {
        SmoothFollow();
    }

    void SmoothFollow()
    {
        _wantedPosition = target.transform.TransformPoint(0, height, -distance);
        transform.position = Vector3.Lerp(transform.position, _wantedPosition, Time.deltaTime * damping);

        Quaternion _wantedRotation = Quaternion.LookRotation(target.transform.position - transform.position, target.transform.up);

        transform.rotation = Quaternion.Slerp(transform.rotation, _wantedRotation, Time.deltaTime * rotationDamping);

        transform.LookAt(target.transform, target.transform.up);
    }
}
