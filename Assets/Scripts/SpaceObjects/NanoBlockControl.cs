﻿using UnityEngine;
using System.Collections;

public class NanoBlockControl : MonoBehaviour 
{

    public GameObject registry;

	// Use this for initialization
	void Start () 
    {
        registry.GetComponent<ObjectsRegistrySystem>().AddObject(ObjectsRegistrySystem.ObjectType.NanoBlock, gameObject);
	}
	
	// Update is called once per frame
	void Update () 
    {
	
	}
}
