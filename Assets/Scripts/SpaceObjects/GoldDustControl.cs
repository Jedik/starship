﻿using UnityEngine;
using System.Collections;

public class GoldDustControl : MonoBehaviour 
{

    public GameObject registry;

    public float amount = 100;

	// Use this for initialization
	void Start () 
    {
        registry.GetComponent<ObjectsRegistrySystem>().AddObject(ObjectsRegistrySystem.ObjectType.GoldDust, gameObject);
	}
	
	// Update is called once per frame
	void Update () 
    {
	
	}

    public float Extraction(float requestedAmount)
    {
        var availableAmount = 0f;
        if (amount < requestedAmount)
        {
            availableAmount = amount;
            Destroy(gameObject);
        }
        else
        {
            availableAmount = requestedAmount;
            amount -= availableAmount;
        }
        return availableAmount;
    }
}
