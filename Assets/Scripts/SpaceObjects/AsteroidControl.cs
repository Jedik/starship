﻿using UnityEngine;
using System.Collections;

public class AsteroidControl : MonoBehaviour {

    public GameObject dust;
    public GameObject registry;

	// Use this for initialization
	void Start () 
    {
        gameObject.GetComponent<HealthControl>().onDestroying += Destroying;
        registry.GetComponent<ObjectsRegistrySystem>().AddObject(ObjectsRegistrySystem.ObjectType.Asteroid, gameObject);
	}
	
	// Update is called once per frame
	void Update () 
    {
        
	}

    void Destroying()
    {
        var tmp = (GameObject)Instantiate(dust, transform.position, transform.rotation);
        tmp.GetComponent<GoldDustControl>().amount = 100;
        Destroy(gameObject);    
    }

    //void OnTriggerEnter(Collider other)
    //{
    //    if (other.name == "PulseLaserProjectile(Clone)")
    //    {
    //        hp -= 1;
    //        Destroy(other.gameObject);
    //    }

    //}
}
