﻿using UnityEngine;
using System.Collections;

public class NanoBotControl : MonoBehaviour 
{

    public GameObject registry;

	// Use this for initialization
	void Start () 
    {
        registry.GetComponent<ObjectsRegistrySystem>().AddObject(ObjectsRegistrySystem.ObjectType.NanoBot, gameObject);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
