﻿using UnityEngine;
using System.Collections;

public class ObjectGeneratorControl : MonoBehaviour 
{
    public GameObject asteroid;
    public GameObject goldDust;
    public GameObject nanoBlock;


	// Use this for initialization
	void Start () 
    {
	
	}
	
	// Update is called once per frame
	void Update () 
    {
	
	}

    public void GenerateAsteroids(Vector3 center, float maxRange, int count)
    {
        for (int i = 0; i < count; i++)
        {
            Instantiate(asteroid, center + Random.insideUnitSphere * maxRange, Random.rotation);
        }
    }

    public void GenerateNanoblocks(Vector3 center, float maxRange, int count)
    {
        for (int i = 0; i < count; i++)
        {
            Instantiate(nanoBlock, center + Random.insideUnitSphere * maxRange, Random.rotation);
        }
    }

    public void GanerateGoldDust(Vector3 center, float maxRange, int count)
    {
        for (int i = 0; i < count; i++)
        {
            Instantiate(goldDust, center + Random.insideUnitSphere * maxRange, Random.rotation);
        }
    }

    //public void GenerateObjectByType(ObjectsRegistrySystem.ObjectType type, Transform position)
    //{ 
        
    //}
}
