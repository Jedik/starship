﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class EnergyControl : MonoBehaviour 
{

    public float energy;
    public float maxEnergy;
    public Slider energyBar;
    //public delegate void MethodContainer();
    //public event MethodContainer onEnergyFinished;

    public AnimationCurve expense;

	// Use this for initialization
	void Start () 
    {
        energy = maxEnergy;
	}
	
	// Update is called once per frame
	void Update () 
    {

	}

    public bool ConsumingEnergy(float amount)
    {
        if (energy >= amount)
        {
            energy -= amount;
            if (energyBar != null)
            {
                energyBar.value = energy / maxEnergy;
            }
            return true;
        }
        return false;
    }

    public bool TurbineNeedEnergy(float speedRatio)
    {
        var tmp = expense.Evaluate(speedRatio) * Time.deltaTime;
        if (energy >= tmp)
        {
            if (energyBar != null)
            {
                energyBar.value = energy / maxEnergy;
            }
            energy -= tmp;
            return true;
        }
        return false;
    }

    public void Recharge(float amount)
    {
        energy += amount;
        if (energy > maxEnergy)
            energy = maxEnergy;
        if (energyBar != null)
        {
            energyBar.value = energy / maxEnergy;
        }       
    }
}
