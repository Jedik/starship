using UnityEngine;
using System.Collections;

public class CameraControl : MonoBehaviour 
{
    public GameObject target3rd;
    public GameObject target1st;

    public GameObject CabinSprite;
    public GameObject Radar;

    bool _1st = true;
	

	
	void Start()
    {

	}
	
	void Update () 
    {

	}

    public void ChangeCamera()
    {
        if (_1st)
        {
            CabinSprite.SetActive(_1st);
            transform.SetParent(target1st.transform, false);
            Radar.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, 60 - Screen.height);
        }
        else
        {
            CabinSprite.SetActive(_1st);
            transform.SetParent(target3rd.transform, false);
            Radar.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, -60);
        }
        _1st = !_1st;
    }
	
}
