﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class PlayerControl : MonoBehaviour
{
    //public GameObject Aim;
    public float fireRate = 0.1f;
    public float maxHealth = 100;
    public float maxEnergy = 100;
	public float maxSpeed = 60;
    public float speed = 0;
    public float speedDiff = 5;
    public GameObject bounds;

    public GameObject registry;

    GameObject[] _turbines;
    GameObject[] _turrets;
    GameObject _extraction;
    
    JoyZoneControl _vController;
    HealthControl _currHealth;
    EnergyControl _currEnergy;
    PlayerResources _resources;

    //bool _autoFire = false;
    bool _speedUp = false;
    bool _speedDown = false;

    float _extractionTime = 0;

    ////////////
    TurretControl.TurretType currentTurret = TurretControl.TurretType.pulse;
    ////////////
	
	void Start()
    {
        //registry.GetComponent<ObjectsRegistrySystem>().AddObject(ObjectsRegistrySystem.ObjectType.Ship, gameObject);
		_turbines = GameObject.FindGameObjectsWithTag("Turbine");
        _turrets = GameObject.FindGameObjectsWithTag("Turret");

        _vController = GameObject.Find("JoyZone").GetComponent<JoyZoneControl>();

        _resources = gameObject.GetComponent<PlayerResources>();

        _currHealth = gameObject.GetComponent<HealthControl>();
        _currHealth.maxHealthPoint = _currHealth.currHealthPoint = maxHealth;

        _currEnergy = gameObject.GetComponent<EnergyControl>();
        _currEnergy.maxEnergy = _currEnergy.energy = maxEnergy;

        TurretsControl();

        TurbinesExhaust(speed / maxSpeed);
	}

    void Update()
    {
        if (_speedUp)
        {
            speed += speedDiff * Time.deltaTime;
            if (speed > maxSpeed)
            {
                speed = maxSpeed;
            }
            TurbinesExhaust(speed / maxSpeed);
        }
        if (_speedDown)
        {
            speed -= speedDiff * Time.deltaTime;
            if (speed < 0)
            {
                speed = 0;
            }
            TurbinesExhaust(speed / maxSpeed);
        }

        if (_extraction != null)
        {
            var amount = (Time.time - _extractionTime) * 20;
            _extractionTime = Time.time;
            _resources.AddGoldDust(_extraction.GetComponent<GoldDustControl>().Extraction(amount));
        }
    }
	
    void LateUpdate()
    {
	    Vector3 mouseMovement = (_vController.vMousePos - (new Vector3(Screen.width, Screen.height, 0) / 2.0f)) * 1;
	    transform.Rotate(new Vector3(-mouseMovement.y, mouseMovement.x, 0) * 0.02f);

        if (_currEnergy.TurbineNeedEnergy(speed / maxSpeed))
        {
            transform.Translate(Vector3.forward * Time.deltaTime * speed);
        }
        else
        {
            if (speed / maxSpeed <= 0.2)
            {
                transform.Translate(Vector3.forward * Time.deltaTime * speed);
            }
            else
            {
                speed = Mathf.Lerp(speed, maxSpeed * 0.2f, Time.deltaTime * 5f);
            }
        }
        
    }
	
	void TurbinesExhaust(float intensity)
    {
		foreach (GameObject turbine in _turbines)
        {
            turbine.GetComponent<LensFlare>().brightness = intensity;
        }
	}

    public void SwitchAutoFire(bool state)
    {
        //_autoFire = state;
        foreach (var turret in _turrets)
        {
            turret.GetComponent<TurretControl>().isShooting = state;
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Asteroids")
        {
            Destroy(other.gameObject);
            _currHealth.HealthChanged(-40);
        }

        if (other.gameObject.tag == "NanoBlock")
        {
            Destroy(other.gameObject);
            _currHealth.HealthChanged(20);
        }

        if (other.gameObject.tag == "NanoBot")
        {
            Destroy(other.gameObject);
            _currEnergy.Recharge(10);
        }

        if (other.gameObject.tag == "GoldDust")
        {
            _extraction = other.gameObject;
            _extractionTime = Time.time;
        }    
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "GoldDust")
        {
            _extraction = null;
            _extractionTime = 0;
        }       
    }


    public void Acceleration(bool state)
    {
        _speedUp = state;
    }

    public void Deceleration(bool state)
    {
        _speedDown = state;
    }


    ///////////////////////////////////

    void TurretsControl()
    {
        foreach (var turret in _turrets)
        {
            turret.GetComponent<TurretControl>().energy = _currEnergy;
            if (turret.GetComponent<TurretControl>().type == currentTurret)
            {
                turret.GetComponent<TurretControl>().isActive = true;
            }
            else
            {
                turret.GetComponent<TurretControl>().isActive = false;
            }
        }
    }

    public void SwitchWeapons()
    {
        switch (currentTurret)
        {
            case TurretControl.TurretType.beam:
                currentTurret = TurretControl.TurretType.rocket;
                break;
            case TurretControl.TurretType.pulse:
                currentTurret = TurretControl.TurretType.beam;
                break;
            case TurretControl.TurretType.rocket:
                currentTurret = TurretControl.TurretType.pulse;
                break;
            default:
                break;
        }
        TurretsControl();
    }

    ///////////////////////////


    public void TargetFind()
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position, transform.forward, out hit, float.PositiveInfinity))
        {
            if (hit.transform.gameObject.layer == LayerMask.NameToLayer("Destructible"))
            {
                bounds.GetComponent<BoundsControl>().TrackTarget(hit.transform.gameObject);

                foreach (var turret in _turrets)
                {
                    if (turret.GetComponent<TurretControl>().type == TurretControl.TurretType.rocket)
                    {
                        turret.GetComponent<TurretControl>().target = hit.transform.gameObject;
                    }
                }
            }
        }
        else
        {
            bounds.GetComponent<BoundsControl>().TrackTarget(null);
        }
    }
}

