﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class HealthControl : MonoBehaviour 
{
    public Slider hpBar;
    public float maxHealthPoint;
    public delegate void MethodContainer();
    public event MethodContainer onDestroying;

    public float currHealthPoint;

	// Use this for initialization
	void Start () 
    {
        currHealthPoint = maxHealthPoint;
	}
	
	// Update is called once per frame
	void Update () 
    {
        
	}

    public void HealthChanged(float diff)
    {
        currHealthPoint += diff;
        
        if (currHealthPoint > maxHealthPoint)
        {
            currHealthPoint = maxHealthPoint;
        }

        if (hpBar != null)
        {
            hpBar.value = currHealthPoint / maxHealthPoint;
        }
        
        if (currHealthPoint <= 0)
        {
            if (onDestroying != null)
            {
                onDestroying();
            }
        }
    }
}
